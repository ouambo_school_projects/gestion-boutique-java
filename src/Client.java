/***********************************************************************
 ** Nom & Prénom : Ouambo Silatchom Sedric
 ** Numéro       : 2712447
 ** Date         : 02/12/2022
 ** Classe       : Client
 ***********************************************************************/

public class Client {

    // Les attributs
    private int identifiant;   // generer automatiquement par les constructeurs
    private String nom;
    private String prenom;
    private String tel;
    private String codePostal;
    private static int num = 0; // Nombre des clients


    // Construteurs.

        // Constructeur sans parametres
    public Client(){

        num++;                        //on incremente la variable de classe num, qui contient le nombre
                                      // de clients crees.

        this.identifiant = num;      //identifiant recoit la valeur courante de la variable de classe num.
    }


        // Constructeur avec tous les parametres
    public Client(String nom, String prenom, String tel, String codePostal) {

        this();                      //incrementation de la variable de classe num, en faisant appel au constructeur sans parametre.
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.codePostal = codePostal;
    }


    // Getter setter

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public static int getNum() {
        return num;
    }

    public static void setNum(int num) {
        Client.num = num;
    }


// Les methodes

    public void afficherClient() {
        System.out.println("Client{" +
                "Identifiant='" + identifiant + '\'' +
                ", Nom='" + nom + '\'' +
                ", Prenom='" + prenom + '\'' +
                ", Tel=" + tel +
                " , CodePostal=" + codePostal +
                '}');
    }

}
