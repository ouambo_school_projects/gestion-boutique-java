/***********************************************************************
 ** Nom & Prénom : Ouambo Silatchom Sedric
 ** Numéro       : 2712447
 ** Date         : 02/12/2022
 ** Classe       : Article
 ***********************************************************************/
public class Article {
    private int reference;                          // Doit etre saisie par l'utilisateur
    private String designation;
    private String couleur = "n/o";                 // s'applique sur certains articles
    private double prixHT;
    private static final double tauxTVA = 13;       //constante partagée au niveau de la classe Article


    // Constructeurs.

        // Constructeur sans parametres
    public Article() {
        // On initialise juste designation parce que les autres attrubuts seront initialisees automatiquement
        this.designation = "";
    }

    // Constructeur ayant 2 parametres : reference et designation
    public Article(int reference, String designation) {
        this.reference = reference;
        this.designation = designation;
    }

        // Constructeur ayant 3 parametres : reference, designation et couleur
    public Article(int reference, String designation, String couleur) {
        this.reference = reference;
        this.designation = designation;
        this.couleur = couleur;
    }

        // Constructeur ayant 3 parametres : reference, designation et prixHT
    public Article(int reference, String designation, double prixHT) {
        this.reference = reference;
        this.designation = designation;
        this.prixHT = prixHT;
    }

        // Constructeur ayant 4 parametres : reference, designation, couleur et prixHT
    public Article(int reference, String designation, String couleur, double prixHT) {
        this.reference = reference;
        this.designation = designation;
        this.couleur = couleur;
        this.prixHT = prixHT;
    }



    // Setter et Getter

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public double getPrixHT() {
        return prixHT;
    }

    public void setPrixHT(double prixHT) {
        this.prixHT = prixHT;
    }


//Les methodes

    // Afficher
    public void afficherArticle() {
        System.out.println("Article {" +
                ": Référence: " + reference + '\t' +
                ", Désignation: " + designation + '\t' +
                ",Couleur: " + couleur + '\t' +
                ",Prix HT: " + prixHT + '\t' +
                ",Le Prix TTC est: " + calculerPrixTTC() +
                '}');
    }


    // Calculer le Prix TTC calculerPrixTTC()
    public double calculerPrixTTC(){
        return prixHT + (prixHT * tauxTVA/100);
    }

}