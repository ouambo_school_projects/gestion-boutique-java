/***********************************************************************
 ** Nom & Prénom : Ouambo Silatchom Sedric
 ** Numéro       : 2712447
 ** Date         : 13/12/2022
 ** Classe       : Panier
 ***********************************************************************/

import java.util.ArrayList;
import java.util.Iterator;

public class Panier {

    private Client client;
    private ArrayList<ElementPanier> listeElementPanier = new ArrayList<ElementPanier>();  // liste des articles et leur quantite
    private int numPanier;                                                     // Numero automatique du panier
    private static int num = 0;                                                // Nombre de paniers crees


    // Constructeur.

    public Panier() {

        num ++;                         // incrementation du nombre de paniers crees

        this.numPanier = num;           // numPanier recoit la valeur courante de la variable num

        this.client = new Client();     // On initialise le champ client pour qu'il ne soit pas null
    }


    // Getter et Setter

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ArrayList<ElementPanier> getListeElementPanier() {
        return listeElementPanier;
    }

    public void setListeElementPanier(ArrayList<ElementPanier> listeElementPanier) {
        this.listeElementPanier = listeElementPanier;
    }

    public int getNumPanier() {
        return numPanier;
    }

    public void setNumPanier(int numPanier) {
        this.numPanier = numPanier;
    }


    //Les methodes

    // xxx  rechercherArticle retourn 0 si pas d'article et retour n le nombre des articles si existe

    // rechercherArticle(Article a)
    public int rechercherArticle(Article a){

            // ici on comparare les references des article du panier a celui de l'article pris
            // pris en argument.
            // Car les articles sont identifies de facon unique par leurs references

        int i = 0;
        while (i < this.listeElementPanier.size()){

            if(this.listeElementPanier.get(i).getArticle().getReference() == a.getReference())

                return this.listeElementPanier.get(i).getQteArticle();   // Lorsqu'on trouve l'article,
                                                                         // on recupere sa quantite et la boucle s'acheve avec return
            else
                i++;                                                    // On incremente i si on n'a pas trouver, pour evoluer dans la boucle
        }
            return 0;                                                   // On retourne 0 si l'article n'est pas trouvee
    }



    // rechercherElementPanier(Article a)
    public ElementPanier rechercherElementPanier(Article a){

            // ici on comparare les references des article du panier a celui de l'article
            // pris en argument.
            // Car les articles sont identifies de facon unique par leurs references

        int i = 0;
        while (i < this.listeElementPanier.size()){

            if(this.listeElementPanier.get(i).getArticle().getReference() == a.getReference())

                return this.listeElementPanier.get(i);   // Lorsqu'on trouve l'article, on retourne l'element et la boucle s'arrete avec return

            else
                i++;                                    // On incremente i si on n'a pas trouver, pour evoluer dans la boucle
        }
            return null;                                // On retourne null si l'article n'est pas trouve
    }



    //  ajouter un article

    //ajouterArticle(Article a, int qte)
    public void ajouterArticle(Article a, int quantite){

        if(this.rechercherElementPanier(a) == null)

            this.listeElementPanier.add(new ElementPanier(a, quantite));            // On ajoute l'article s'il n'existe pas deja

        else

            this.rechercherElementPanier(a).ajouterQuantite(quantite);              // On ajoute la quantite de l'article s'il existe deja
    }


    // supprimer  un article
    // supprimerArticle(Article a)
    public void supprimerArticle(Article a){


        if(this.rechercherElementPanier(a) != null)

            this.listeElementPanier.remove(this.rechercherElementPanier(a));      //On supprime l'article si on le trouve
    }


    // modifier Quantite un article

    //  modifierQuantiteArticle(Article a , int qte)
    public void modifierQuantiteArticle(Article a, int quantite){

        if(this.rechercherElementPanier(a) != null)

            this.rechercherElementPanier(a).setQteArticle(quantite);         // On modifie la quantite d'un article si on le trouve
    }


    // retourner le montant d'un panier

    //getMontantPanier()
    public double getMontantPanier(){

        // L'idee est de parcourir la liste des articles du panier en calculant et en sommant les prixTTC * quantite

        int i=0, qte = 0;

        double prixttc = 0, montant =0;

        while (i < this.listeElementPanier.size()){

            qte = this.listeElementPanier.get(i).getQteArticle();                        //recuperation de la quantite del'article

            prixttc = this.listeElementPanier.get(i).getArticle().calculerPrixTTC();     //prix TTC de l'article

            montant += qte * prixttc;                                                    // Calcul du montant de l'article se trouvant a l'indice i

            i++;
        }
        return montant;
    }


    // vider le panier
    //viderPanier()
    public void viderPanier(){
        this.listeElementPanier.clear();
    }


    // xxxxafficher le panier
    public void afficherPanier() {
        System.out.println("------------------------- Panier -----------------------------");
        if (this.listeElementPanier.size() == 0)
            System.out.println("** Le panier numero "+this.getNumPanier()+"  est Vide **");
        else {
            System.out.println("** Le panier numero "+this.getNumPanier()+"  contient les articles suivants : **");
            for (int i = 0; i < listeElementPanier.size(); i++) {
                listeElementPanier.get(i).getArticle().afficherArticle();
                System.out.println("  Quantite : " + listeElementPanier.get(i).getQteArticle());
            }
        }
    }

}