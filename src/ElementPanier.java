/***********************************************************************
 ** Nom & Prénom : Ouambo Silatchom Sedric
 ** Numéro       : 2712447
 ** Date         : 02/12/2022
 ** Classe       : ElementPanier
 ***********************************************************************/

public class ElementPanier {
    private Article article;
    private int qteArticle = 0;

    //Constructeurs.

        // Constructeurs sans parametres
    public ElementPanier(){
        this.article = new Article();
    }

        // Constructeur avec tous les parametres
    public ElementPanier(Article article, int qteArticle) {
        this.article = article;
        this.qteArticle = qteArticle;
    }


    //Setter et Getter

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getQteArticle() {
        return qteArticle;
    }

    public void setQteArticle(int qteArticle) {
        this.qteArticle = qteArticle;
    }


    //methodes

    // afficher
    public void afficherElementPanier() {
        this.article.afficherArticle();
        System.out.println("Quantite est :" + qteArticle);
    }

    // ajouter une quantite
    public void ajouterQuantite(int quantite){
        this.qteArticle += quantite;
    }  // On ajoute la nouvelle valeur à celle existant deja.
}
